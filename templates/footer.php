<section id="footer_widget_content" class="sage-footer sage-section sage-section-md-padding" style="padding-top: 90px; padding-bottom: 90px; background-color: rgb(46, 46, 46);">
	<div class="container">
		<div class="row">
	    		<?php dynamic_sidebar('sidebar-footer'); ?>
		</div>
	</div>
</section>
<footer class="content-info sage-small-footer sage-section sage-section-nopadding" id="footer1-18" style="background-color: rgb(50, 50, 50); padding-top: 1.75rem; padding-bottom: 1.75rem;">
   	<p class="text-xs-center">
		Copyright (с) 2016  <a href="#" rel="nofollow"> Hummingbird Web Solutions Pvt Ltd </a>All Rights Reserved. 
	</p>
</footer>
